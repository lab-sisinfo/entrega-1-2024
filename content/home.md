---
date: 2024-10-06T00:50:44-03:00
draft: false
---

<section class="home-container">
  <div class="home-intro">
    <img src="/images/image-home.jpg" alt="Foto de Luiz Henrique Piffer Marques" class="profile-image">
    <div class="home-text">
      <h1>Seja bem-vindo ao meu currículo online!</h1>
      <p>Meu nome é <strong>Luiz Henrique Piffer Marques</strong>, e este site foi desenvolvido para apresentar de forma detalhada e organizada a minha trajetória profissional e pessoal. Aqui, você poderá conhecer um pouco mais sobre minha carreira, incluindo os projetos nos quais atuei, as qualificações e cursos que concluí, bem como as habilidades técnicas e competências que desenvolvi ao longo do tempo.</p>
      <p>O objetivo deste espaço é não apenas compartilhar minha experiência e conhecimentos adquiridos, mas também destacar minha dedicação e comprometimento em cada etapa da minha jornada profissional.</p>
    </div>
  </div>
</section>
