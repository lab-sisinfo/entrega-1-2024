---
title: "Acadêmica"
date: 2024-10-06T00:00:00-03:00
draft: false
image: /images/academico.png
---

<section class="academic-container">
  <div class="academic-content">
    <h1>Acadêmica</h1>
    <p>Durante minha jornada acadêmica, participei de diversos projetos de extensão e pesquisa:</p>
    <h3>Monitoria da Disciplina PCC3100</h3>
    <div class="academic-item">
      <img src="/images/monitoria.jpg" alt="Siemens NX Monitoria" class="academic-image">
      <p>Atuei como monitor na disciplina PCC3100 - Representação Gráfica de Projetos, parte do ciclo básico da Engenharia na Poli. Fui responsável por auxiliar três turmas no aprendizado do software Siemens NX, além de aplicar e corrigir atividades e prestar suporte no desenvolvimento do projeto final da disciplina.</p>
    </div>
    <h3>Representação Discente na Comissão de Graduação</h3>
    <div class="academic-item">
      <img src="/images/CG.jpeg" alt="Representação Discente" class="academic-image">
      <p>Fui eleito representante discente (RD) na Comissão de Graduação da Poli (CG), onde tive a oportunidade de contribuir em debates sobre adaptações e mudanças curriculares, além da criação de métricas e processos de graduação. Também participei das discussões sobre a curricularização da extensão e as Atividades Acadêmicas, Artísticas, Culturais e Científicas (AAAC's). </p>
      <p>Além disso, pude participar do COBENGE - Congresso Brasileiro de Engenharia e Educação, que contou com palestras e discussões sobre as novas DCN's.</p>
    </div>
    <h3>Gestão do Grêmio Politécnico da USP</h3>
    <div class="academic-item">
      <img src="/images/Gremio.jpg" alt="Grêmio Politécnico da USP" class="academic-image">
      <p>Atuei por dois anos na gestão do Grêmio Politécnico da USP, ocupando os cargos de Diretor de Projetos e Diretor Geral. O Grêmio é a maior entidade estudantil da América Latina, representando 6 mil alunos de graduação e pós-graduação. Como Diretor de Projetos, organizei eventos, palestras e semanas acadêmicas que visavam complementar a formação dos alunos. Como Diretor Geral, liderei uma equipe de 25 diretores e 45 funcionários, sendo responsável pela administração de todas as áreas do Grêmio, incluindo suas três empresas.</p>
    </div>
  </div>
</section>
