---
title: "Profissional"
date: 2024-10-06T00:00:00-03:00
draft: false
image: /images/profissional.png

---

<section class="professional-container">
  <div class="professional-content">
    <h1>Profissional</h1>
    <p><strong>Cargo (atual):</strong> Estagiário de BI e FP&A</p>
    <p><strong>Empresa:</strong> GCB Investimentos - Ventures</p>
    <p>Atuo como estagiário de BI e FP&A no grupo GCB Investimentos, focado nas Ventures:</p>
    <ul>
      <li><strong class="highlight">Adiante Recebíveis:</strong> Empresa de antecipação de crédito B2B.</li>
      <li><strong class="highlight">PeerBR:</strong> Empresa de tokenização de ativos do mercado financeiro.</li>
    </ul>
    <p>Integrando o time de operações, que oferece suporte a ambas as empresas, minhas responsabilidades são divididas em duas áreas principais:</p>
    <ul>
      <li>Business Intelligence (BI): Realizo análises de dados para os times de produto e growth, gerando insights e apoiando na tomada de decisões estratégicas sobre os produtos e serviços fornecidos pelas ventures.</li>
      <li>FP&A (Planejamento Financeiro): Dou suporte no backoffice, cuidando do planejamento financeiro das ventures e garantindo a gestão eficiente dos recursos.</li>
    </ul>
  </div>
</section>
