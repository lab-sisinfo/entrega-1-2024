---
title: "Outros"
date: 2024-10-06T00:00:00-03:00
draft: false
image: /images/outros.png
---
<section class="other-projects-container">
  <div class="other-content">
    <h1>Outros</h1>
    <p>Aqui estão alguns projetos que desenvolvi ao longo da minha trajetória e que tiveram grande impacto na minha formação pessoal e profissional:</p>
    <h3>The Coopers</h3>
    <div class="image-container">
        <img src="/images/coopers.jpg" alt="Equipe The Coopers" class="other-projects-image">
    </div>
    <p>The Coopers foi uma equipe de robótica de competição que fundei, junto com mais seis amigos, durante o ensino fundamental. Vale ressaltar que estudávamos em uma escola municipal de São Caetano do Sul, que não possuía um programa de robótica. Com o apoio do professor de Informática e uma parceria com a Lego Education, formamos essa equipe, onde assumi a responsabilidade pela Programação do robô.</p>
    <p>Participamos de dois campeonatos municipais, conquistando o prêmio de "Melhor Design e Programação" no primeiro ano e o prêmio "Geral" no segundo. No terceiro ano, nos classificamos para a OBR (Olimpíada Brasileira de Robótica), onde avançamos para a fase estadual e recebemos o prêmio de melhor equipe estreante.</p>
    <p>Essa experiência foi fundamental para mim, pois foi meu primeiro contato com trabalho em equipe, desenvolvimento de projetos e liderança.</p>
  </div>
</section>
