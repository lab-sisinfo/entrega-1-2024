---
date: 2024-10-06T00:50:18-03:00
draft: false
---

<section class="about-container">
  <div class="about-intro">
    <div class="about-text">
      <h1>Sobre mim</h1>
      <p>Sou Luiz Henrique Piffer Marques, um jovem brasileiro com uma paixão por engenharia e inovação. Atualmente, estou cursando Engenharia Mecatrônica na Escola Politécnica da Universidade de São Paulo (USP), com previsão de conclusão em 2026. Desde cedo, desenvolvi uma sólida base técnica com minha formação como Técnico em Mecatrônica pela ETEC Jorge Street.</p>
      <p>Minha trajetória tem sido marcada por minha dedicação a atividades extracurriculares e meu envolvimento em projetos de liderança. Entre o final de 2021 e 2023, atuei no Grêmio Politécnico da USP, onde fui Diretor Geral, liderando uma equipe de 70 pessoas e gerenciando uma entidade com um faturamento médio de 2 milhões de reais ao ano. Também exerci o cargo de Diretor de Projetos, coordenando eventos acadêmicos como a Semana de Inovação, que reuniu patrocínios de mais de 70 mil reais.</p>
      <p>Além da minha experiência em liderança, também atuei como monitor na disciplina de Representação Gráfica de Projetos, onde tive a oportunidade de ensinar a ferramenta Siemens NX a alunos do primeiro ano da faculdade.</p>
      <p>Atualmente sou estagiário de BI e FP&A na área de ventures do grupo GCB. Lá atuo com análise de dados para as duas fintechs do grupo, participando de decisões de estratégia de produtos e planejamento financeiro.</p>
      <p>Com conhecimentos em Python, AutoCAD, Inventor, SolidWorks, MatLab, HTML, SQL, Verilog e modelagem, tenho uma abordagem prática e criativa para resolver desafios técnicos, sempre buscando aprender e evoluir.</p>
    </div>
  </div>
</section>
