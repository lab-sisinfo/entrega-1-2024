---
date: 2024-10-06T00:50:18-03:00
draft: false
---



<section class="education-section">
 <p><strong>Formação Acadêmica</strong></p>
  <!-- Formação 1 -->
  <div class="education-item">
    <img src="/images/poli.jpg" alt="Escola Politécnica da Universidade de São Paulo" class="education-image">
    <div class="education-details">
      <p><strong>02/2021 a 12/2025 (previsão)</strong></p>
      <p><strong>Escola Politécnica da Universidade de São Paulo</strong> – Engenharia Mecatrônica</p>
    </div>
  </div>

  <!-- Formação 2 -->
  <div class="education-item">
    <img src="/images/etec.jpeg" alt="ETEC Jorge Street" class="education-image">
    <div class="education-details">
      <p><strong>02/2018 a 12/2020</strong></p>
      <p><strong>ETEC Jorge Street</strong> – Técnico em Mecatrônica</p>
    </div>
  </div>

</section>
